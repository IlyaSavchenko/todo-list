import { ActionsList } from "./list.js";

const btnAdd = document.querySelector('.add-action');
const btnRemove = document.querySelector('.remove-action');
const name = document.querySelector('.action-name');
const actionsListUI = document.querySelector('.actions-list');
const actionsList = new ActionsList([]);

if (btnAdd) {
    btnAdd.addEventListener('click', () => {
        createAction();
    });
}

function createAction() {
    if (name.value && !checkValue(name.value)) {
        actionsList.addAction(name.value);
        clearField();
        updateListUI();
    } else {
        console.log('такой элемент уже существует');
        return;
    }
    
}

function updateListUI() {
    if (actionsList.getData.length) {
        actionsListUI.innerHTML = '';
        actionsList.getData.forEach(action => {
            const elem = document.createElement('div');
            const checkbox = document.createElement('input');
            const name = document.createElement('span');
            elem.classList.add('action');
            actionsListUI.appendChild(elem);
            checkbox.type = 'checkbox';
            checkbox.classList.add('action-checkbox');
            checkbox.onchange = chooseElement;
            name.classList.add('action-name');
            name.innerText = action.name;
            name.ondblclick = removeAction;
            if (action.done) {
                checkbox.checked = true;
                name.style.textDecoration = 'line-through';
            }
            elem.append(checkbox, name);
        });
    } else {
        actionsListUI.innerHTML = 'В списке дел пусто';
    }
}

function clearField() {
    name.value = '';
}

function removeAction($event) {
    actionsList.removeAction($event.target.innerText);
    updateListUI();
}

function chooseElement($event) {
    const name = $event.target.nextElementSibling.innerText;
    const index = actionsList.getData.findIndex(item => item.name === name);
    
    if ($event.target.checked) {
        $event.target.nextElementSibling.style.textDecoration = 'line-through';
    } else {
        $event.target.nextElementSibling.style.textDecoration = 'none';
    }

    actionsList.getData[index].done = !actionsList.getData[index].done;
    updateCheckedList(index, $event.target.checked);
}

function checkValue(value) {
    return actionsList.getData.find(action => action.name === value);
}

function updateCheckedList(checkedIndex, checked) {
    const startCheckedPos = actionsList.getData.findIndex(action => action.done);
    const tempItem = actionsList.getData[checkedIndex];

    actionsList.getData.splice(checkedIndex, 1);
    if (checked) {
        actionsList.getData.push(tempItem);
    } else {
        if (startCheckedPos === 0) {
            actionsList.getData.splice(0, 0, tempItem);
        } else if (startCheckedPos > 0) {
            actionsList.getData.splice(startCheckedPos, 0, tempItem);
        } else {
            actionsList.getData.splice(actionsList.getData.length, 0, tempItem);
        }
    }
    updateListUI();
}