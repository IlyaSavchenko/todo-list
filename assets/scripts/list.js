import { Action } from "./action.js";

export class ActionsList {
    constructor(info) {
        this.actionsList = info;
    }

    get getData() {
        return this.actionsList;
    }

    addAction(name) {
        const action = new Action(name, false);
        const startCheckedPos = this.actionsList.findIndex(action => action.done === true);

        startCheckedPos === -1 ? this.actionsList.push(action) : this.actionsList.splice(startCheckedPos, 0, action);
    }

    removeAction(name) {
        const index = this.actionsList.findIndex(action => action.name === name);
        this.actionsList.splice(index, 1);
    }

    clearList() {
        this.actionsList = [];
    }
}