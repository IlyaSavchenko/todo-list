export class Action {

    constructor(name, done) { 
        this.name = name;
        this.done = done;
    }
}